<?php
/*
* Template name:Page default
*/
get_header(); // This fxn gets the header.php file and renders it ?>
<section id="breadcrumb-wrapper" class="breadcrumb-w-img">
    <div class="breadcrumb-overlay"></div>
    <div class="breadcrumb-content">
        <div class="container">
            <div class="inner text-center">
                <div class="breadcrumb-big">
                    
                    <h2>
                       <?php the_title(); ?>
                    </h2>
                    
                </div>
                <div class="breadcrumb-small">
                         <?php the_breadcrumb(); ?>                  
                </div>
            </div>
        </div>
    </div>
</section>
<div class="container">
    <div class="row">
        
        <div class="col-md-12">
            <?php if(have_posts()):while(have_posts()):the_post(); ?>
            
            <h2 class="page-title"><span><?php the_title(); ?></span></h2>
            <div class="entry_content">
                <?php the_content(); ?>
                
            </div>
            
            <div class="pagebox likebox">
                <div class="fb-like" data-href="<?php the_permalink(); ?>" data-layout="standard" data-action="like" data-show-faces="true" data-share="true"></div>
            </div>
            <?php endwhile; endif; ?>
            
        </div>
        
    </div>
    
</div>
<?php get_footer(); // This fxn gets the footer.php file and renders it ?>