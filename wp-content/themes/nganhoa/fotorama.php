<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script> <!-- 33 KB -->

<link  href="http://cdnjs.cloudflare.com/ajax/libs/fotorama/4.6.4/fotorama.css" rel="stylesheet"> <!-- 3 KB -->

<script src="http://cdnjs.cloudflare.com/ajax/libs/fotorama/4.6.4/fotorama.js"></script> <!-- 16 KB -->

<?php $foto = get_field('gallery'); ?>

<div class="fotorama"  data-nav="thumbs" <?php if(wp_is_mobile()){ echo '';} else { ?>data-width="710" data-height="474" <?php } ?> >
	<?php foreach ($foto as $key => $image) { ?>

		<a href="<?php echo $image['url']; ?>"><img src="<?php echo $image['sizes']['thumbmd'] ?>" width="150" height="100" data-caption="<?php echo $image['title']; ?>"></a>

	<?php } ?>
</div>