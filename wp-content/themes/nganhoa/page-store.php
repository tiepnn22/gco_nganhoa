<?php
/*
* Template name:Page store
*/
get_header(); ?>

<?php
    $api_google_map = get_field('api_google_map');

    // Lấy thông tin các store
    $terms = get_terms('local_category', array(
        // 'parent'=> 0,
        'hide_empty' => false
    ) );
?>
<script src="https://polyfill.io/v3/polyfill.min.js?features=default"></script>
<script src="<?php echo $api_google_map; ?>" defer></script>


<!-- <link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri();?>/css/select2.css"> -->
<!-- <script type="text/javascript" src="<?php echo get_stylesheet_directory_uri();?>/js/select2.js"></script> -->
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/css/select2.min.css">
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>







<section id="breadcrumb-wrapper" class="breadcrumb-w-img">
    <div class="breadcrumb-overlay"></div>
</section>

<div class="container">
    <div class="row">
        <div class="col-md-9 content_col">
            <?php if(have_posts()):while(have_posts()):the_post(); ?>
                <h2 class="single-title"><span><?php the_title(); ?></span></h2>
                <div class="entry_content">
                    <?php the_content(); ?>
                </div>
            <?php endwhile; endif; ?>

            <div class="page-store">
                <div class="page-store-cat">
                    <select id="danhmuc">
                        <!-- <option value="">Chọn khu vực</option> -->
                        <?php
                            $i = 0;
                            foreach($terms as $term){
                                echo "<option class='hover-option' value='".$term->term_id."'>".$term->name."</option>";
                                if($i == 0) { $term_id_first = $term->term_id; }
                            $i++; }
                        ?>
                    </select>
                </div>

                <?php
                    // lấy id chuyên mục đầu tiên cho vào query
                    $query =  new WP_Query( array(
                            'post_type' => 'store',
                            'tax_query' => array(
                                                array(
                                                        'taxonomy' => 'local_category',
                                                        'field' => 'id',
                                                        'terms' => $term_id_first,
                                                        'operator'=> 'IN'
                                                 )),
                            'showposts'=> 100,
                            'order' => 'DESC',
                            'orderby' => 'date'
                     ) );
                ?>
                <div class="page-store-ajax">
                    <div class="row">
                        <div class="col-md-4 page-store-post">
                            <div class="page-store-post-content">

                                <?php
                                    $j = 0;
                                    if($query->have_posts()) : while ($query->have_posts() ) : $query->the_post();
                                        $post_id = get_the_ID();
                                        $post_title = get_the_title($post_id);
                                        $post_content = get_the_content($post_id);
                                        
                                        $store_marker_lat = get_field('store_marker_lat', $post_id);
                                        $store_marker_lng = get_field('store_marker_lng', $post_id);
                                ?>
                                    <div class="item">
                                        <div class="title">
                                            <i class="fa fa-map-marker icon"></i>
                                            <a href="javascript:void(0)" data-title="<?php echo $post_title; ?>" data-content="<?php echo wpautop( $post_content); ?>" data-lat="<?php echo $store_marker_lat; ?>" data-lng="<?php echo $store_marker_lng; ?>">
                                                <h4><?php echo $post_title; ?></h4>
                                            </a>
                                        </div>
                                        <div class="desc">
                                            <?php echo wpautop( $post_content ); ?>
                                        </div>
                                    </div>
                                <?php $j++; endwhile; wp_reset_postdata(); else: echo ''; endif;?>
                                
                            </div>
                            <div class="page-store-post-count">
                                Tìm thấy <span class="page-store-post-number"><?php echo $j; ?></span> chi nhánh
                            </div>

                        </div>
                        <div class="col-md-8 page-store-map">
                            <div id="map" style=""></div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div class="col-md-3 sidebar_col">
            <?php get_sidebar(); ?>
        </div>
        
    </div>
</div>

<?php get_footer(); ?>


<!--ajax store-->
<script type="text/javascript">
    //map init
    function initMap(map_array) {
        console.log(map_array);

        const map = new google.maps.Map(document.getElementById("map"), {
            zoom: 13,
            center: { lat: parseFloat(map_array[0][2]), lng: parseFloat(map_array[0][3]) },
        });
        setMarkers(map,map_array);
    }
    //map multi Markers
    function setMarkers(map,map_array) {
        for (let i = 0; i < map_array.length; i++) {
            
            const map_array_row = map_array[i];

            const marker = new google.maps.Marker({
                position: { lat: parseFloat(map_array_row[2]), lng: parseFloat(map_array_row[3]) },
                map,
                draggable: true,
                animation: google.maps.Animation.DROP,
                title: map_array_row[0]+map_array_row[1]
            });

            // Dòng text
            const contentString = '<div class="title-map">'+map_array_row[0]+'</div>'+map_array_row[1];
            const infowindow = new google.maps.InfoWindow({
                content: contentString,
            });
            marker.addListener("click", () => {
                infowindow.open(map, marker);
            });
            //auto bật tiêu đề marker
            // infowindow.open(map, marker);

        }
    }
    //ajax show google map init theo category store đầu tiên
    $(window).on('load', function() {
        var iddanhmuc = $("#danhmuc").val();
        $.ajax({
            type: 'POST',
            url: ajaxurl,
            data: {
                action : 'InitDanhmuc',
                iddanhmuc : iddanhmuc,
            },
            success:function(response){
                data = jQuery.parseJSON(response);

                //map
                initMap(data.array);
            }
        });
    });
    //ajax show google map click từng chi nhánh
    function click_store(){
        $(".page-store-post .page-store-post-content .item a").on('click', function(){
            var title = $(this).data("title");
            var content = $(this).data("content");
            var lat = $(this).data("lat");
            var lng = $(this).data("lng");

            const map_array = [
                [title, content, lat, lng],
            ];

            //map
            initMap(map_array);
        });
    }
    click_store();


    $(document).ready(function(){
        if ($('#danhmuc').length > 0) {
            $('#danhmuc').select2();
        }

        //ajax show all chi nhánh trong store + google map
        $("#danhmuc").on('change', function() {
            var iddanhmuc = $("#danhmuc").val();
            $.ajax({
                type: 'POST',
                url: ajaxurl,
                data: {
                    action : 'ChangeDanhmuc',
                    iddanhmuc : iddanhmuc,
                },
                success:function(response){
                    data = jQuery.parseJSON(response);
                    $(".page-store-post-content").html(data.result);
                    $(".page-store-post-number").text(data.count);

                    //map
                    initMap(data.array);
                    click_store();
                }
            });
        });
    });
</script>

<style type="text/css" media="screen">
    .page-store {}
    .page-store .page-store-cat {
        text-align: center;
        margin: 0 0 30px 0;
    }
    .page-store .page-store-cat select {
        width: 48%;
        border: 1px solid #000;
        padding: 10px;
        margin: 30px 0;
    }

    .page-store .page-store-ajax {}
    .page-store .page-store-ajax .page-store-post {
        display: flex;
        display: -webkit-flex;
        flex-wrap: wrap;
        -moz-flex-wrap: wrap;
        -webkit-flex-wrap: wrap;
        -o-flex-wrap: wrap;
        -ms-flex-wrap: wrap;
    }
    .page-store .page-store-ajax .page-store-post .page-store-post-count {
        order: 1;
        -moz-order: 1;
        -webkit-order: 1;
        -o-order: 1;
        -ms-order: 1;
        font-size: 20px;
        color: #000;
        width: 100%;
    }
    .page-store .page-store-ajax .page-store-post .page-store-post-content {
        order: 2;
        -moz-order: 2;
        -webkit-order: 2;
        -o-order: 2;
        -ms-order: 2;
        width: 100%;
        max-height: 368px;
        overflow: auto;
    }

    .page-store .page-store-ajax .page-store-post .page-store-post-content .item {}
    .page-store .page-store-ajax .page-store-post .page-store-post-content .item .title {
        display: inline-block;
        width: 100%;
        padding: 10px 0;
    }
    .page-store .page-store-ajax .page-store-post .page-store-post-content .item .title .fa {
        float: left;
        width: 30px;
        text-align: center;
        font-size: 22px;
        color: red;
    }
    .page-store .page-store-ajax .page-store-post .page-store-post-content .item .title a {
        float: left;
        width: calc(100% - 30px);
    }
    .page-store .page-store-ajax .page-store-post .page-store-post-content .item .title a h4 {
        padding: 0;
        margin: 0;
        font-size: 18px;
        font-weight: 700;
        color: #333333;
    }
    .page-store .page-store-ajax .page-store-post .page-store-post-content .item .title a:hover h4 {
        color: red;
    }
    .page-store .page-store-ajax .page-store-map p {
        margin-bottom: 0;
    }
    #map {
        height: 100%;
        max-height: 400px;
    }
    body .select2-container--open .select2-dropdown--below {
        border-top: 1px solid #aaa;
    }
    body .select2-container {
        margin: 0;
        position: relative;
        display: inline-block;
        zoom: 1;
        width: 300px !important;
    }
    @media screen and (max-width: 992px) {
        .page-store .page-store-ajax .page-store-post .page-store-post-content {
            max-height: 215px;
        }
    /*    .page-store .page-store-ajax .row {
            display: flex;
            display: -webkit-flex;
            flex-wrap: wrap;
            -moz-flex-wrap: wrap;
            -webkit-flex-wrap: wrap;
            -o-flex-wrap: wrap;
            -ms-flex-wrap: wrap;
        }
        .page-store .page-store-ajax .page-store-post {
            order: 2;
            -moz-order: 2;
            -webkit-order: 2;
            -o-order: 2;
            -ms-order: 2;
            width: 100%;
            padding-top: 15px;
            padding-bottom: 15px;
        }
        .page-store .page-store-ajax .page-store-map {
            order: 1;
            -moz-order: 1;
            -webkit-order: 1;
            -o-order: 1;
            -ms-order: 1;
            width: 100%;
            height: 400px;
        }*/
    }
</style>


<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/js/select2.min.js"></script>

