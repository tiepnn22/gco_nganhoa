<!DOCTYPE html <?php language_attributes();?> >
<head>
	<meta name="google-site-verification" content="-TWdGzuQR7iiWLhhZUvSN4BK2dKiP1a228KuqxACi_c" />
	<meta charset="<?php bloginfo('charset'); ?>" />
	<meta content="width=device-width, initial-scale=1.0" name="viewport">
	<meta name="description" content="" />
	<title>
		<?php echo title(); ?>
	</title>
	<meta name="keywords" content="<?php echo title(); ?>" />
	<meta name="robots" content="noodp,index,follow"/>
	<meta name='revisit-after' content='1 days' />
	<link rel="shortcut icon" href="<?php the_field('favicon','option'); ?>">
	<link href='https://fonts.googleapis.com/css?family=Roboto:400,300,500,700,900&subset=latin,vietnamese,latin-ext' rel='stylesheet' type='text/css'>
	<link rel="profile" href="" />
	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
	<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/jquery.mmenu.all.css">
	<?php ?>
	<?php // Loads HTML5 JavaScript file to add support for HTML5 elements in older IE versions. ?>
	<!--[if lt IE 9]>
	<script src="<?php echo get_template_directory_uri(); ?>/js/html5.js" type="text/javascript"></script>
	<![endif]-->
	<script src="<?php echo get_template_directory_uri(); ?>/js/jquery.min.js" type="text/javascript"></script>
	<script src="<?php echo get_template_directory_uri(); ?>/js/jquery.mmenu.all.js" type="text/javascript"></script>
	<script>
	var ajaxurl = '<?php echo admin_url('admin-ajax.php'); ?>';
	</script>
	<script type="text/javascript">
	jQuery(document).ready(function( $ ) {
	$("#menu").mmenu({
		extensions: ['theme-dark'],
		});
	});
	</script>
	<?php wp_head(); ?>
<!-- Facebook Pixel Code -->
<script>
  !function(f,b,e,v,n,t,s)
  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
  n.queue=[];t=b.createElement(e);t.async=!0;
  t.src=v;s=b.getElementsByTagName(e)[0];
  s.parentNode.insertBefore(t,s)}(window, document,'script',
  'https://connect.facebook.net/en_US/fbevents.js');
  fbq('init', '945034829258346');
  fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
  src="https://www.facebook.com/tr?id=945034829258346&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-B95BKDPN0P"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-B95BKDPN0P');
</script>
</head>
<meta name="msvalidate.01" content="629E5EB5227B1A0D66F65FC4C73CEB3F" />

<body <?php body_class(); ?>>
	<div id="fb-root"></div>
<script async defer crossorigin="anonymous" src="https://connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v7.0&appId=2774013792831452&autoLogAppEvents=1"></script>
	<div id="header">
		<div class="container">
			<div class="row">
				<div id="page" class="hidden-md hidden-lg">
					<div class="header">
						<a href="#menu"><span><i class="fa fa-bars"></i></span></a>
					</div>
				</div>
				<div class="head1">
					<div class="row">
						<div class="col-md-5">
							<div class="menu_left">
								 <?php wp_nav_menu(array('theme_location' => 'left')); ?>
							</div>
						</div>
						<div class="col-md-2">
							<div class="logo">
								<a href="<?php echo home_url('/') ?>"><img class="img-responsive" src="<?php the_field('logo','option'); ?>" alt="Dược phẩm chức năng"></a>
							</div>
						</div>
						<div class="col-md-5">
							<div class="menu_right">
								 <?php wp_nav_menu(array('theme_location' => 'right')); ?>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-11">
					<nav id="topmenu" class="navbar navbar-default ">
							<?php
							wp_nav_menu(array(
							'menu' => 'primary',
							'theme_location' => 'primary',
							'depth' => 4,
							'container' => 'div',
							'container_class' => 'collapse navbar-collapse pd0',
							'container_id' => 'bsmenu',
							'menu_class' => 'nav navbar-nav',
							'fallback_cb' => 'wp_bootstrap_navwalker::fallback',
							'walker' => new wp_bootstrap_navwalker())
							);
							?>
					</nav>
				</div>
				<div class="col-sm-1 col-md-1 search_box">
					<div>
					    	<!-- <label><i class="fa fa-search"></i></label> -->
					    	<div class="box_hovers">
					    		<form action="<?php echo home_url(); ?>/" id="searchform" method="get">
						    		<input type="text" id="s" name="s" value="" />
						    		<input type="hidden" name="post_type" value="product">
						        	<input type="submit" value="" id="searchsubmit" />
					        	</form>
					    	</div>
					    </div>
				</div>
			</div>
		</div>
	</div>
	<!--end header-->
	<div id="main_body">