<?php get_header(); ?>
<section id="breadcrumb-wrapper" class="breadcrumb-w-img">
    <div class="breadcrumb-overlay"></div>
</section>
<div class="container">
    <div class="row">
        <div class="col-md-9 content_col">
            <?php if (have_posts()) : ?>
            <?php $post = $posts[0]; // Hack. Set $post so that the_date() works. ?>
            
            <!-- <?php fl_optitle($post, 'page-title'); ?> -->
            <h1 class="page-title"><?php echo single_cat_title(); ?></h1>

            <?php if (is_tax()) { ?>
            <div class="tax_desc">
                <?php echo term_description(); ?>
            </div>
            <?php } ?>
            <div id="result">
            <?php $cur = get_queried_object_id();?>
            <?php $loop = new WP_Query(array('posts_per_page' =>12,'tax_query'=>array(array('taxonomy'=>'product_cat','field'=>'term_id','terms'=>$cur)) ,'paged' => get_query_var('paged') ? get_query_var('paged') : 1 )); ?>
            <div class="row tax_box">
                <?php while ($loop->have_posts() ) : $loop->the_post();?>
                <div class="col-md-4">
                    <?php include 'template/product-item.php'; ?>
                </div>
                <?php endwhile; ?>
            </div>
            
            <div class="paginate">
                <?php
                global $wp_query;
                $big = 999999999; // need an unlikely integer
                echo paginate_links(array(
                'base' => str_replace($big, '%#%', esc_url(get_pagenum_link($big))),
                'format' => '?paged=%#%',
                'current' => max(1, get_query_var('paged')),
                'total' => $loop->max_num_pages,
                'prev_text'          => __('&larr;'),
                'next_text'          => __('&rarr;'),
                ));
                ?>
            </div>
            <?php else : ?>
            
            <!-- <h2 class="page-title">Không có bài viết nào</h2> -->
            <h1 class="page-title"><?php echo single_cat_title(); ?></h1>

            <?php endif; ?>
            </div>
        </div>
        <div class="col-md-3 sidebar_col">
            <?php get_sidebar('product'); ?>
        </div>
    </div>
</div>
<?php get_footer(); ?>