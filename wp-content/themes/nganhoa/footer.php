</div> <!-- endmain-body -->
<div id="to_top">
<a href="#" class="btn btn-primary"><i class="fa fa-angle-double-up"></i></a>
</div>
<div id="footer">
    <div class="overlay"></div>
<div class="container">
    <div class="row">
        <div class="col-md-5 ftcol ft1">
            <h3 class="sub-title text-uppercase ft_slogan"><?php the_field('slogan','option'); ?></h3>
            <div class="ft_logo">
                <img src="<?php the_field('logo','option'); ?>">
            </div>
            <div class="ft_gt">
                <?php the_field('gt_ft','option'); ?>
            </div>
            <div class="box-social">
                            <ul class="clear">
                                <a href="<?php the_field('bct','option'); ?>" rel="nofollow">
                                    <img width="150px" src="<?php echo get_template_directory_uri();?>/images/da-thong-bao-bo-cong-thuong.png">
                                </a>
                                <a href="#" rel="nofollow" title="DMCA.com Protection Status" class="dmca-badge">
                </a>
                <script src="https://images.dmca.com/Badges/DMCABadgeHelper.min.js"> </script>
                            </ul>
                        </div>
        </div>
        <div class="col-md-4 ftcol ft2">
            <h3 class="sub-title text-uppercase">Thông tin liên hệ</h3>
            <div class="ft_bx">
                <li><i class="fa fa-map-marker"></i> Đ/c: <?php the_field('address','option') ?></li>
                <li><i class="fa fa-map-marker"></i> VP : <?php the_field('office','option') ?></li>
                <li><i class="fa fa-phone"></i> Hotline: <span class="hotline"><a href="tel:<?php the_field('phone','option') ?>"><?php the_field('phone','option') ?></a></span></li>
                <li><i class="fa fa-phone"></i> Hotline: <span class="hotline"><a href="tel:<?php the_field('hotline','option') ?>"><?php the_field('hotline','option') ?></a></span></li>
                <li><i class="fa fa-envelope"></i> Email: <span><a href="mailto:<?php the_field('email','option') ?>"><?php the_field('email','option') ?></a></span></li>
            </div>
        </div>
        <div class="col-md-3 ftcol ft3">
            <h3 class="sub-title text-uppercase">Hỗ trợ khách hàng</h3>
            <div class="ft_bx">
                <?php wp_nav_menu(array('theme_location' => 'footer_menu')); ?>
            </div>
        </div>
    </div>
</div>
</div>
<div class="copyright">
<div class="container"><p>Copyright &copy; 2015 by nganhoavietnam.vn. All right reserved</p></div>
</div>
<div class="support">
    <div class="support-item " style="background: #c2b597">
        <a href="tel:<?php the_field('hotline','option'); ?>" style="color: #261511">
            <img src="<?php echo get_template_directory_uri();?>/images/phoneicon-1.png"><?php the_field('hotline','option'); ?>
        </a>
    </div>
    <div class="support-item " style="background: #c2b597">
        <a href="https://m.me/<?php the_field('facebook','option'); ?>" style="color: #261511">
            <img src="<?php echo get_template_directory_uri();?>/images/messicon.png">Messenger
        </a>
    </div>
    <div class="support-item " style="background: #c2b597">
        <a href="https://zalo.me/<?php the_field('zalo','option'); ?>" style="color: #261511">
            <img src="<?php echo get_template_directory_uri();?>/images/zaloicon.png">Zalo
        </a>
    </div>
</div>
<?php wp_footer(); ?>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css" />
<script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js"></script>
<nav id="menu">    
    <?php
    wp_nav_menu(array(
    'menu' => 'primary',
    'theme_location' => 'primary',
    'depth' => 4)
    );
    ?>
</nav>
</body>
</html>