<?php get_header(); // This fxn gets the header.php file and renders it ?>
<link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/css/jquery.fancybox.min.css" />
<section id="breadcrumb-wrapper" class="breadcrumb-w-img">
    <div class="breadcrumb-overlay"></div>
</section>
<div class="head_product">
    <div class="container">
 
            <div class="row">
                <div class="fixedpro col-md-7">
                    <div class="box_foto">
                        <div class="img_zoom">
                                <?php $foto = get_field('gallery'); ?>
                                <?php foreach ($foto as $key => $ftv) { ?>
                                    <a data-fancybox="gallery" href="<?php echo $ftv['url']; ?>"><?php if($key==0){ ?><div class="icon_zoom" data-toggle="tooltip" data-placement="top" title="zoom"><i class="fa fa-expand"></i></div><?php } ?></a>
                               <?php  } ?>
                            </div>
                        <?php include 'fotorama.php'; ?>
                    </div>
                </div>
                <div class="meta_pro col-md-5">
                    <div class="product_detail">
                        <?php if(have_posts()):while(have_posts()):the_post(); ?>
                        <h2 class="single-title-product product-title"><span><?php the_title(); ?></span></h2>
                        <?php endwhile;endif; wp_reset_query(); ?>
                        <div class="box_meta ">
                            <div class="meta_col">
                                <?php $product_id =get_the_ID();
                                $p_nomal = get_field('price',$product_id);
                                $p_sale = get_field('sale',$product_id);
                                if($p_sale){ $price_end = product_price($p_sale);} else { $price_end = product_price($p_nomal); }
                                ?>
                                
                                <div class="pro_info">
                                    <p>Mã sản phẩm: <?php the_field('sku'); ?> </p>
                                    <p>Chất liệu: <?php the_field('chatlieuu'); ?>  </p>
                                    <p>Kích thước : <?php the_field('size'); ?> </p>
                                    <p>Tình trạng hàng: <span style="color:#3ea322;"><?php the_field('tinhtrang') ?></span> </p>
                                    <p>Bảo hành: <?php the_field('baohanh'); ?> </p>
                                    <p>Miễn phí vận chuyển nội thành </p>
                                </div>
                                <!-- <ul class="all-tt">
                                    <li class="gia gia-ban">
                                        <?php echo product_price($p_sale); ?> <span class="vnd">VNĐ</span>
                                    </li>
                                    <li class="gia gia-goc clear">
                                        <div class="col-gia-goc">
                                            Giá gốc: <del><strong><?php echo product_price($p_nomal); ?> VNĐ </strong></del>
                                        </div>
                                        <div class="col-gia-tiet-kiem">
                                            Tiết kiệm: <?php echo product_price($p_nomal-$p_sale); ?> VNĐ
                                        </div>
                                    </li>
                                </ul> -->
                                <div class="box_right_pro">
                                    <div class="order">
                                      <a data-fancybox data-type="ajax" data-src="<?php echo home_url();?>/dat-hang/?pid=<?php echo $product_id;?>" href="javascript:;">
                                            <span class="cols1">CHỌN MUA NGAY</span>
                                            <span class="cols2">Giảm thêm 100k khi chuyển khoản trước tiền hàng</span>
                                        </a>
                                    </div>
                                    <div class="proDetail-send loadFormTel" style="position:relative;">
                                        <div class="content">
                                            <?php echo do_shortcode('[contact-form-7 id="627" title="Gọi cho tôi"]'); ?>
                                        </div>
                                    </div>
                                    <div class="tel box_lh">
                                        <a href="tel:<?php the_field('hotline','option'); ?>">
                                            <span class="cols1"><?php the_field('hotline','option'); ?></span>
                                            <span class="cols2">Hotline tư vấn mua hàng</span>
                                        </a>
                                    </div>
                                    <div class="face">
                                        <a href="http://m.me/KangarooVietnam.vn" target="_blank">
                                            <span class="cols1">Chat facebook</span>
                                            <span class="cols2">/facebook</span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box_entry">
                <div class="row">
                    <div class="col-md-9 content_col">
                        <div class="content_info">
                            <h3 class="sb_title"><span>Chi tiết sản phẩm</span></h3>
                            <div class="entry_content">
                                <?php the_content(); ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 sidebar_col">
                        <?php get_sidebar('product'); ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="related_box">
            <div class="container">
                <h3 class="sb_title"><span>Sản phẩm liên quan</span></h3>
                <div class="related_posts media_posts posts">
                    <?php $cur = get_queried_object()->term_id;
                    $terms = get_the_terms( $post->ID, 'product_cat' );
                    if ( !empty( $terms ) ){
                    $term = array_shift( $terms );
                    }
                    query_posts(array('post_type' => 'product','posts_per_page' =>-1,'post__not_in' => array(get_the_ID()),'tax_query' =>array(array('taxonomy'=>'product_cat','field' => 'term_id','terms'=>$term->term_id))));?>
                    
                    <div class="relate_owl owl-carousel">
                        <?php if(have_posts()):while(have_posts()):the_post(); ?>
                        <div class="item">
                            <?php include 'template/product-item.php'; ?>
                        </div>
                        <?php endwhile; endif; wp_reset_query(); ?>
                    </div>
                    
                </div>
            </div>
        </div>
 
    </div>
</div>
<?php get_footer(); // This fxn gets the footer.php file and renders it ?>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>
<script>
jQuery(document).ready(function($) {
$('.relate_owl').owlCarousel({loop:true,margin:25,nav:true,autoplay:true,autoplayTimeout:2000,autoplayHoverPause:true,
responsive:{0:{items:1},480:{items:2},1000:{items:4,nav:true}}
});
$(".owl-prev").html('<i class="fa fa-angle-left"></i>');
$(".owl-next").html('<i class="fa fa-angle-right"></i>');
});
</script>
<script type="text/javascript">
$(".addnum").click(function(){
$(this).prev(".number").val(parseInt($(this).prev(".number").val())+1);
});
$(".subnum").click(function(){
if(parseInt($(this).next(".number").val()) >1){
$(this).next(".number").val(parseInt($(this).next(".number").val())-1);
};
});
</script>
<script src="<?php echo get_template_directory_uri();?>/js/jquery.fancybox.min.js"></script>
<script>
jQuery(document).ready(function($) {
  $('[data-toggle="tooltip"]').tooltip();   
});
</script>