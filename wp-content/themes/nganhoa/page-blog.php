<?php
/*
* Template name:Page Blog
*/
get_header(); // This fxn gets the header.php file and renders it ?>
<section id="breadcrumb-wrapper" class="breadcrumb-w-img">
    <div class="breadcrumb-overlay"></div>
    <div class="breadcrumb-content">
        <div class="container">
            <div class="inner text-center">
                <div class="breadcrumb-big">
                    
                    <h2>
                       <?php the_title(); ?>
                    </h2>
                    
                </div>
                <div class="breadcrumb-small">
                         <?php the_breadcrumb(); ?>                  
                </div>
            </div>
        </div>
    </div>
</section>
<div class="container">
    <div class="row">
        
        <div class="col-md-9 content_col">
            <div class="blog-single-item">
                <div class="section-title clearfix">
                    <div class="title">
                        <h2>Tin tức</h2>
                    </div>
                    <div class="viewmore">
                        <a href="/category/tin-tuc">Xem tất cả <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                    </div>
                    <div class="line"></div>
                </div>
                <div class="blog-single-slider grid">
                <?php query_posts(array('post_type' => 'post', 'posts_per_page' =>-1, 'category_name' =>'tin-tuc')); ?>
                     <div class="owl-carousel owl-theme blog_news">
                     <?php if (have_posts()):while (have_posts()):the_post(); ?>
                        <?php $date = get_the_date(); ?>
                        <div class="item">
                            <div class="news_item">
                                <div class="thumb">
                                    <a href="<?php the_permalink();?>"><?php the_post_thumbnail('thumbnews', array('class' => 'img-responsive')); ?></a>
                                </div>
                                <div class="news_title">
                                    <a href="<?php the_permalink(); ?>"><h3 class="news_title_slider"><?php the_title(); ?></h3></a>
                                </div>
                                <div class="sub_single_title">
                                    <div class="date"><i class="fa fa-calendar" aria-hidden="true"></i> <?php echo $date; ?></div>
                                    <div class="comment"><i class="fa fa-commenting-o" aria-hidden="true"></i> <span class="fb-comments-count fb_comments_count_zero" data-href="https://suplo-thuoc.myharavan.com/blogs/news/tac-dung-cua-dong-trung-ha-thao-trong-viec-chua-benh" fb-xfbml-state="rendered"><span class="fb_comments_count">0</span></span></div>
                                    <div class="aritcle-author"><i class="fa fa-user" aria-hidden="true"></i> <?php the_author(); ?></div>
                                </div>
                                <div class="news_exrept">
                                    <?php echo fl_short_desc(get_the_ID(),20,'...'); ?>
                                </div>
                            </div>
                        </div>
                     <?php endwhile;endif; ?>
                     </div>
                     <?php  wp_reset_query(); ?> 
                </div>
            </div>

            <!-- end box tintuc slider -->

            <div class="blog_banner">
                <img src="<?php echo ot_get_option('blog_banner'); ?>">
            </div>
            <div class="blog-single-item">
                <div class="section-title clearfix">
                    <div class="title">
                        <h2>Review Sản phẩm</h2>
                    </div>
                    <div class="viewmore">
                        <a href="/category/tin-tuc">Xem tất cả <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                    </div>
                    <div class="line"></div>
                </div>
                <div class="blog-single-slider grid">
                    <?php query_posts(array('post_type' => 'post', 'posts_per_page' =>-1, 'category_name' =>'tin-tuc')); ?>
                         <div class="owl-carousel owl-theme blog_review">
                         <?php if (have_posts()):while (have_posts()):the_post(); ?>
                            <?php $date = get_the_date(); ?>
                            <div class="item">
                                <div class="news_item">
                                    <div class="thumb">
                                        <a href="<?php the_permalink();?>"><?php the_post_thumbnail('thumbnews', array('class' => 'img-responsive')); ?></a>
                                    </div>
                                    <div class="news_title">
                                        <a href="<?php the_permalink(); ?>"><h3 class="news_title_slider"><?php the_title(); ?></h3></a>
                                    </div>
                                    <div class="sub_single_title">
                                        <div class="date"><i class="fa fa-calendar" aria-hidden="true"></i> <?php echo $date; ?></div>
                                        <div class="comment"><i class="fa fa-commenting-o" aria-hidden="true"></i> <span class="fb-comments-count fb_comments_count_zero" data-href="https://suplo-thuoc.myharavan.com/blogs/news/tac-dung-cua-dong-trung-ha-thao-trong-viec-chua-benh" fb-xfbml-state="rendered"><span class="fb_comments_count">0</span></span></div>
                                        <div class="aritcle-author"><i class="fa fa-user" aria-hidden="true"></i> <?php the_author(); ?></div>
                                    </div>
                                    <div class="news_exrept">
                                        <?php echo fl_short_desc(get_the_ID(),20,'...'); ?>
                                    </div>
                                </div>
                            </div>
                         <?php endwhile;endif; ?>
                         </div>
                         <?php  wp_reset_query(); ?> 
                </div>
            </div>
            <div class="blog-single-item">
                <div class="section-title clearfix">
                    <div class="title">
                        <h2>Sống khỏe mỗi ngày</h2>
                    </div>
                    <div class="viewmore">
                        <a href="/category/tin-tuc">Xem tất cả <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                    </div>
                    <div class="line"></div>
                </div>
                <div class="noidung"></div>
            </div>
            <div class="blog-single-item">
                <div class="section-title clearfix">
                    <div class="title">
                        <h2>Bản tin nội bộ</h2>
                    </div>
                    <div class="viewmore">
                        <a href="/category/tin-tuc">Xem tất cả <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                    </div>
                    <div class="line"></div>
                    <div class="noidung"></div>
                </div>
            </div>
        </div>
        <div class="col-md-3 sidebar_col">
            <?php get_sidebar(); ?>
        </div>
        
    </div>
    
</div>
<?php get_footer(); // This fxn gets the footer.php file and renders it ?>
<script src="<?php echo get_template_directory_uri();?>/js/owl.carousel.min.js"></script>
<script>
    jQuery(document).ready(function($){
    $('.blog_news,.blog_review').owlCarousel({
    loop:true,
    margin:10,
    nav:true,
    autoplay:true,
    autoplayTimeout:2000,
    autoplayHoverPause:true,
    responsive:{
        0:{
            items:1
        },
        480:{
            items:1
        },
        1000:{
            items:2,
        }
        }
    });
    $(".owl-prev").html('<i class="fa fa-angle-left"></i>');
    $(".owl-next").html('<i class="fa fa-angle-right"></i>');
});
</script>