<?php

/* 
 * Template Name: Contact page
 */

get_header();
?>
<section id="breadcrumb-wrapper" class="breadcrumb-w-img">
    <div class="breadcrumb-overlay"></div>
</section>
<div class="container contact_page">
    <div class="row">
        
        <div class="col-md-12">
            <?php if(have_posts()):while(have_posts()):the_post(); ?>
            
            <h2 class="page-title"><span><?php the_title(); ?></span></h2>

            <div class="entry_content">
                <div class="col-md-6">
                   
                        <h3 class="sub-title text-uppercase ft_slogan"><?php the_field('slogan','option'); ?></h3>
                        <ul class="infos">
                            <li><strong><i class="fa fa-map-marker"></i> Địa chỉ</strong>: <span><?php the_field('address','option') ?></span></li>
                            <li><strong><i class="fa fa-phone"></i> Điện thoại</strong>: <span class="hotline"><a href="tel:<?php the_field('phone','option') ?>"><?php the_field('phone','option') ?></a></span></li>
                            <li><strong><i class="fa fa-envelope"></i> Email</strong>: <span><a href="mailto:<?php the_field('email','option') ?>"><?php the_field('email','option') ?></a></span></li>
                        </ul>
                        <div class="ggmap">
                            <?php the_content(); ?>
                        </div>
                    
                </div>
                <div class="col-md-6">
                     <div class="contact_form qcol">
                        <?php echo do_shortcode('[contact-form-7 id="66" title="Liên hệ"]'); ?>
                    </div>
                </div>
            </div>

          
            <?php endwhile; endif; ?>
            
        </div>
        
    </div>
    
</div>

<?php get_footer(); ?>

