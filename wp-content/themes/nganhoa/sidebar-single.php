<div class="sb_content">
     
    <div class="sb_box newest">
        <h3 class="sb_title"><span>Bài viết mới nhất</span></h3>
        <div class="media_posts posts">
            <?php query_posts(array('post_type' => 'post', 'posts_per_page' => 5)); ?>
            <?php if(have_posts()):while(have_posts()):the_post(); ?>
                <?php include 'template/media_post.php'; ?>
            <?php endwhile; endif; wp_reset_query(); ?>
        </div>
    </div>
     
</div>
