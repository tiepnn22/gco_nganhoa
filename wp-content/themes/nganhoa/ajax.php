<?php 
function price_range(){
      $min_price = isset($_POST['min_price']) ? $_POST['min_price'] : '';
    $max_price = isset($_POST['max_price']) ? $_POST['max_price'] : '';
     $query_price[] = array(
                'key' => 'price',
                'value' =>array($min_price,$max_price),
                'compare' => 'BETWEEN',
                'type' => 'numeric'
            );
    query_posts(array(
        'post_type' => 'product',
        'posts_per_page' =>-1,
        'meta_query' => $query_price
    )); ?>
    <div class="row">
    <?php if (have_posts()):while (have_posts()):the_post(); ?>
            <div class="col-md-4">
                <div class="product_item">
                    <div class="thumb">
                        <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('thumbpro', array('class' => 'img-responsive')); ?></a>
                        <?php $product_id =get_the_ID();
                        $p_nomal = get_field('price',$product_id);
                        $p_sale = get_field('sale',$product_id);
                        ?>
                    </div>
                    <div class="item_info">
                        <h3 class="product_title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                    </div>
                    <p class="star">
                        <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i>
                    </p>
                    <div class="box_price">
                        <div class="col-haflt sale">
                            <del>
                            <?php echo product_price($p_nomal); ?> đ
                            </del>
                        </div>
                        <div class="col-haflt">
                            <?php echo product_price($p_sale); ?> đ
                        </div>
                    </div>
                </div>
            </div>
        
    <?php endwhile;endif;wp_reset_query(); ?>
    </div>
    <?php die();
}

 
add_action( 'wp_ajax_price_range', 'price_range' );
add_action( 'wp_ajax_nopriv_price_range', 'price_range' );
 ?>
