<?php get_header(); // This fxn gets the header.php file and renders it ?>
<section id="breadcrumb-wrapper" class="breadcrumb-w-img">
    <div class="breadcrumb-overlay"></div>
</section>
<div class="container">
    <div class="row">
        <div class="col-md-9 content_col">
            <?php if(have_posts()):while(have_posts()):the_post(); ?>
            <h1 class="single-title"><span><?php the_title(); ?></span></h1>
            <div class="entry_content">
                <div class="sub_single_title">
                    <div class="date"><i class="fa fa-calendar" aria-hidden="true"></i> <?php the_date(); ?></div>
                    <div class="comment"><i class="fa  fa-eye" aria-hidden="true"></i>  <span class="fb_comments_count"><?php echo getPostViews(get_the_ID()); ?></span></div>
                    <div class="aritcle-author"><i class="fa fa-user" aria-hidden="true"></i> <?php the_author(); ?></div>
                </div>
                <div class="intro_blog">
                    <?php // echo get_the_excerpt(); ?>
                </div>
                <?php setPostViews(get_the_ID()); ?>
                <?php the_content(); ?>
            </div>
            <?php endwhile;endif; ?>
        </div>
    <div class="col-md-3 sidebar_col">
        <?php get_sidebar();?>
    </div>
</div>
<div class="related_box">
    <div class="container">
        <div class="row">
            <h3 class="related_title text-left"><span>Bài viết liên quan</span></h3>
            <div class="related_posts">
                <?php $postcat = get_the_category($post->ID); $cat = $postcat[0]->term_id;
                query_posts(array('cat'=>$cat,'post__not_in' => array(get_the_ID()), 'posts_per_page' => 4));?>
                <div class="related_post_list row">
                     <?php if(have_posts()):while(have_posts()):the_post(); ?>
                    <div class="col-md-3">
                        <?php include 'template/col-cat.php'; ?>
                    </div>
                     <?php endwhile; endif; wp_reset_query(); ?>
                </div>
                
            </div>
        </div>
    </div>
</div>
<?php get_footer(); // This fxn gets the footer.php file and renders it