<div class="social">

    <a class="e7 delay-2" href="<?php the_field('facebook','option'); ?>" rel="nofollow" target="_blank"><i class="fa fa-facebook  fa-fw"></i></a>

    <a class="e7 delay-3" href="<?php the_field('twitter','option'); ?> " rel="nofollow" target="_blank"><i class="fa fa-twitter  fa-fw"></i></a>

    <a class="e7 delay-4" href="<?php the_field('google_plus','option'); ?> " rel="nofollow" target="_blank"><i class="fa fa-google-plus  fa-fw"></i></a>

    <a class="e7 delay-5" href="<?php the_field('youtube','option'); ?> " rel="nofollow" target="_blank"><i class="fa fa-youtube  fa-fw"></i></a>

</div>

<style>

.social a {

    border-radius: 100%;

    width: 41px;

    height: 41px;

    padding: 11px 0;

    margin: 0 10px 10px 0;

    text-align: center;

    display: inline-block;

    -webkit-transition: all .4s ease;

    -moz-transition: all .4s ease;

    -o-transition: all .4s ease;

    transition: all .4s ease;

    /* border-radius: 3px; */

border:1px solid #8a3c18;

    background: #fff;

    font-size: 19px;

    color: #8a3c18;

            -webkit-transition: background 0.3s, color 0.3s;

    -moz-transition: background 0.3s, color 0.3s;

    transition: background 0.3s, color 0.3s;

}

.social a:hover{

    background: #8a3c18!important;

    color:#fff;

}

.social a i.fa{

            -webkit-transition: background 0.3s, color 0.3s;

    -moz-transition: background 0.3s, color 0.3s;

    transition: background 0.3s, color 0.3s;

}

.social a:hover i.fa {

  -webkit-animation: toTopFromBottom 0.3s forwards;

  -moz-animation: toTopFromBottom 0.3s forwards;

  animation: toTopFromBottom 0.3s forwards;

color:#fff;

}

@-webkit-keyframes toTopFromBottom {

  49% {

    -webkit-transform: translateY(-100%);

  }

  50% {

    opacity: 0;

    -webkit-transform: translateY(100%);

  }

  51% {

    opacity: 1;

  }

}

@-moz-keyframes toTopFromBottom {

  49% {

    -moz-transform: translateY(-100%);

  }

  50% {

    opacity: 0;

    -moz-transform: translateY(100%);

  }

  51% {

    opacity: 1;

  }

}

@keyframes toTopFromBottom {

  49% {

    transform: translateY(-100%);

  }

  50% {

    opacity: 0;

    transform: translateY(100%);

  }

  51% {

    opacity: 1;

  }

}

</style>