<?php get_header();?>
<section id="breadcrumb-wrapper" class="breadcrumb-w-img">
    <div class="breadcrumb-overlay"></div>
</section>
<div id="main-content" class="page_search">
    <div class="container">
            <h3 class="page-title">Kết quả tìm kiếm:</h3>
            <div class="row">
                <?php if(have_posts()) :  while(have_posts()) : the_post(); ?> 
                    <div class="col-md-3">
                        <?php include 'template/product-item.php'; ?>
                    </div>            
                <?php endwhile; else : ?>
                <h3> Nothing found!</h3>
            </div>
        </div>

    <?php endif; ?>
    </div>
</div>
<?php get_footer(); ?>