<?php
/* Template Name:Sản phẩm
*/
get_header(); // This fxn gets the header.php file and renders it ?>
<section id="breadcrumb-wrapper" class="breadcrumb-w-img">
    <div class="breadcrumb-overlay"></div>
</section>
<div class="container">
        <div class="page_sp">
            <?php $loop = new WP_Query(array('post_type'=>'product','posts_per_page' =>8)); ?>
                    <div class="row">
                        <?php while ($loop->have_posts() ) : $loop->the_post();?>
                            <div class="col-md-3 col-xs-6">
                                <?php include 'template/product-item.php'; ?>
                            </div>
                        <?php endwhile; ?>
                    </div>
                <?php wp_reset_query(); ?>
        </div>
</div>
<?php get_footer(); // This fxn gets the footer.php file and renders it ?>