<?php
//convert đ to VNĐ
add_filter('woocommerce_currency_symbol', 'change_existing_currency_symbol', 10, 2);
 
function change_existing_currency_symbol( $currency_symbol, $currency ) {
 switch( $currency ) {
 case 'VND': $currency_symbol = ' vnđ'; break;
 }
 return $currency_symbol;
}
/*Woocommerce minicart*/
add_filter('woocommerce_add_to_cart_fragments', 'woocommerce_header_add_to_cart_fragment');
function woocommerce_header_add_to_cart_fragment( $fragments ) {
 global $woocommerce;
 ob_start();
 ?>
 <a class="cart-contents" href="<?php  echo wc_get_cart_url(); ?>"><i class="fa fa-shopping-cart fa-fixed"></i><span class="pro_num"><?php
            echo sprintf(_n('%d', '%d', $woocommerce->cart->cart_contents_count, 'woothemes'), $woocommerce->cart->cart_contents_count);?></span></a>
 <?php
 $fragments['a.cart-contents'] = ob_get_clean();
 return $fragments;
}

//field checkout
add_filter('woocommerce_checkout_fields', 'iz_custom_address_field');
function iz_custom_address_field($fields){
    unset($fields['billing']['billing_company']);
    unset($fields['billing']['billing_postcode']);
    return $fields;
}

add_filter('woocommerce_output_related_products_args', 'iz_num_products_related');
function iz_num_products_related($args){
    $args['posts_per_page'] = 4;
    return $args;
}


add_action('wp_ajax_product_search', 'iz_ajax_product_search');
add_action('wp_ajax_nopriv_product_search', 'iz_ajax_product_search');
function iz_ajax_product_search(){
    if(isset($_POST['action']) && $_POST['action'] == 'product_search'){
        $key = $_POST['key'];
        global $query_string;
        $args = array('post_type'=>'product','posts_per_page'=>6,  's'=>$key);
        $search = new WP_Query($args);
        $result = '';
        if($search->have_posts()):while ($search->have_posts()):$search->the_post();
        global $product;
        $result .= '
           <li>
                <a href="'.  get_permalink().'">
                    <div class="thumbnail">
                        '.  get_the_post_thumbnail().'
                    </div>
                    <div class="info">
                        <h4>'.  get_the_title().'</h4>
                        <div class="price">Giá: '.$product->get_price_html().'</div>
                    </div>
                </a>
            </li> 
        ';
        endwhile;        wp_reset_query();        wp_reset_postdata();
        echo $result.'<li class="more-search">Xem tất cả</li>';
        else:
            echo 'Không có sản phẩm nào!';
        endif;
        die();
    }
}


// sale percent
add_filter('woocommerce_sale_flash', 'iz_sale_percent', 10, 3);
function iz_sale_percent($text, $post, $_product){
    $from = $_product->regular_price;
    $to = $_product->price;
    if($from == $to || !$to) return '';
    $percent = round(($from - $to)/$from*100);
    $text = $from>$to?'-':'+';
    return '<span class="onsale">'.$text.' '.$percent.'%'.'</span>';
}


// Display Fields
add_action( 'woocommerce_product_options_general_product_data', 'woo_add_custom_general_fields' );

// Save Fields
add_action( 'woocommerce_process_product_meta', 'woo_add_custom_general_fields_save' );

function woo_add_custom_general_fields() {
 
  global $woocommerce, $post;
  
  echo '<div class="options_group">';
  
  woocommerce_wp_text_input(array(
      'id' => '_cmml',
      'label' => 'Nồng độ',
      
  ));
  
  echo '</div>';
	
}
function woo_add_custom_general_fields_save( $post_id ){
	
	// Text Field
	$woocommerce_text_field = $_POST['_cmml'];
	if( !empty( $woocommerce_text_field ) )
		update_post_meta( $post_id, '_cmml', esc_attr( $woocommerce_text_field ) );
		
	
}
add_filter( 'loop_shop_per_page', 'new_loop_shop_per_page', 20 );

function new_loop_shop_per_page( $cols ) {
  $cols = 12;
  return $cols;
}

add_filter( 'woocommerce_output_related_products_args', 'jk_related_products_args' );
  function jk_related_products_args( $args ) {
    $args['posts_per_page'] = 4; // 4 related products
    $args['columns'] = 4; // arranged in 2 columns
    return $args;
}

//optimuz woocommerce
add_action( 'wp_enqueue_scripts', 'child_manage_woocommerce_styles', 99 );
 
function child_manage_woocommerce_styles()
{
//remove generator meta tag
     remove_action( 'wp_head', array( $GLOBALS['woocommerce'], 'generator' ) );
 
     //first check that woo exists to prevent fatal errors
     if ( function_exists( 'is_woocommerce' ) )
     {
          //dequeue scripts and styles
          if ( ! is_woocommerce() && ! is_cart() && ! is_checkout() )
          {
               wp_dequeue_style( 'woocommerce_frontend_styles' );
               wp_dequeue_style( 'woocommerce_fancybox_styles' );
               wp_dequeue_style( 'woocommerce_chosen_styles' );
               wp_dequeue_style( 'woocommerce_prettyPhoto_css' );
               wp_dequeue_script( 'wc_price_slider' );
               wp_dequeue_script( 'wc-single-product' );
               wp_dequeue_script( 'wc-add-to-cart' );
               wp_dequeue_script( 'wc-cart-fragments' );
               wp_dequeue_script( 'wc-checkout' );
               wp_dequeue_script( 'wc-add-to-cart-variation' );
               wp_dequeue_script( 'wc-single-product' );
               wp_dequeue_script( 'wc-cart' );
               wp_dequeue_script( 'wc-chosen' );
               wp_dequeue_script( 'woocommerce' );
               wp_dequeue_script( 'prettyPhoto' );
               wp_dequeue_script( 'prettyPhoto-init' );
               wp_dequeue_script( 'jquery-blockui' );
               wp_dequeue_script( 'jquery-placeholder' );
               wp_dequeue_script( 'fancybox' );
               wp_dequeue_script( 'jqueryui' );
          }
     }
}
//count view product
add_filter( 'woocommerce_product_add_to_cart_text', 'woo_custom_cart_button_text' );                                // < 2.1
add_filter( 'woocommerce_product_single_add_to_cart_text', 'woo_custom_cart_button_text' );  
add_filter( 'woo_archive_custom_cart_button_text', 'woo_custom_cart_button_text' );  // 2.1 +
  
function woo_custom_cart_button_text() {
  
        return __( 'Đặt hàng', 'woocommerce' );
  
}

//change stt 
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_price', 10 );
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_excerpt', 20 );
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40 );
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_sharing', 50 );

add_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_excerpt', 10 );
add_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 50 );
add_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_sharing', 20 );
add_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_price', 40 );
add_action( 'after_setup_theme', 'yourtheme_setup' );

function yourtheme_setup() {
add_theme_support( 'wc-product-gallery-zoom' );
add_theme_support( 'wc-product-gallery-lightbox' );
add_theme_support( 'wc-product-gallery-slider' );
}
//custom quanlity
// Add this to your theme's functions.php
function kia_add_script_to_footer(){
    if( ! is_admin() ) { ?>
    <script>
    jQuery(document).ready(function($){
    $('.quantity').on('click', '.plus', function(e) {
        $input = $(this).prev('input.qty');
        var val = parseInt($input.val());
        var step = $input.attr('step');
        step = 'undefined' !== typeof(step) ? parseInt(step) : 1;
        $input.val( val + step ).change();
    });

    $('.quantity').on('click', '.minus', 
        function(e) {
        $input = $(this).next('input.qty');
        var val = parseInt($input.val());
        var step = $input.attr('step');
        step = 'undefined' !== typeof(step) ? parseInt(step) : 1;
        if (val > 0) {
            $input.val( val - step ).change();
        } 
    });
});
</script>
<?php }
}
add_action( 'wp_footer', 'kia_add_script_to_footer' );


add_filter( 'woocommerce_product_tabs', 'woo_new_product_tab' );
function woo_new_product_tab( $tabs ) {
  
  // Adds the new tab
  
  $tabs['cs_tab'] = array(
    'title'   => __( 'Tổng quan sản phẩm', 'woocommerce' ),
    'priority'  => 50,
    'callback'  => 'woo_new_product_tab_content'
  );
  return $tabs;
}
function woo_new_product_tab_content() {
  the_field('csbh');
}
add_filter( 'woocommerce_product_tabs', 'woo_reorder_tabs', 98 );
function woo_reorder_tabs( $tabs ) {
  $tabs['description']['priority'] = 5;      // Description second
  $tabs['cs_tab']['priority'] = 10; // Additional information third
   $tabs['reviews']['priority'] =52;     // Reviews first
 // unset( $tabs['reviews'] ); 
  return $tabs;
}
add_filter( 'woocommerce_product_tabs', 'woo_rename_tabs', 98 );
function woo_rename_tabs( $tabs ) {
  $tabs['description']['title'] = __( 'Thông số kĩ thuật' );
  $tabs['reviews']['title'] = __( 'Coments' ); 
  return $tabs;
}
add_filter('woocommerce_get_price_html', 'changeFreePriceNotice', 10, 2);
 
function changeFreePriceNotice($price, $product) {
  if (( $price == wc_price( 0.00 ) )|| (empty($price)))
    return '<span class="zero_price">Liên Hệ</span>';
  else
    return $price;
}

// Change number or products per row to 3
add_filter('loop_shop_columns', 'loop_columns');
if (!function_exists('loop_columns')) {
  function loop_columns() {
    return 3; // 3 products per row
  }
}

add_filter( 'woocommerce_product_tabs', 'woo_remove_product_tabs', 98 );

function woo_remove_product_tabs( $tabs ) {
    unset( $tabs['reviews'] );      // Remove the reviews tab
    return $tabs;
}
// autp update price
add_action( 'WC_Product_Variable', 'woocommerce_total_product_price', 'set priority here' );
function woocommerce_total_product_price() {
    global $woocommerce, $product;
    // let's setup our divs
    echo sprintf('<div id="product_total_price" style="margin-bottom:20px;display:none">%s %s</div>',__('Product Total:','woocommerce'),'<span class="price">'.$product->get_price().'</span>');
    echo sprintf('<div id="cart_total_price" style="margin-bottom:20px;display:none">%s %s</div>',__('Cart Total:','woocommerce'),'<span class="price">'.$product->get_price().'</span>');
    ?>
        <script>
            jQuery(function($){
                var price = <?php echo $product->get_price(); ?>,
                    current_cart_total = <?php echo $woocommerce->cart->cart_contents_total; ?>,
                    currency = '<?php echo get_woocommerce_currency_symbol(); ?>';

                $('[name=quantity]').change(function(){
                    if (!(this.value < 1)) {
                        var product_total = parseFloat(price * this.value),
                        cart_total = parseFloat(product_total + current_cart_total);

                        $('#product_total_price .price').html( currency + product_total.toFixed(2));
                        $('#cart_total_price .price').html( currency + cart_total.toFixed(2));
                    }
                    $('#product_total_price,#cart_total_price').toggle(!(this.value <= 1));

                });
            });
        </script>
    <?php
}

