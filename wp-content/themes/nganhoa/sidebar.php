<?php $bn = get_field('Sidebar','option'); ?>
<?php if($bn){ 
 foreach ($bn as $key => $bnv) { if($key==0){?>
<div class="sb_banner">
    <a href="<?php echo $bnv['link']; ?>">
        <img src="<?php echo $bnv['banner']; ?>" alt="banner" class="img-responsive img-full">
    </a>
</div>
<?php } } } ?>
<div class="sb_content">
    <div class="sb_box newest">
        <h3 class="sb_title">Sản phẩm tiêu biểu</h3>
        <div class="media_posts posts">
            <?php query_posts(array('post_type' => 'product', 'posts_per_page' => 8)); ?>
            <?php if(have_posts()):while(have_posts()):the_post(); ?>
                <?php include 'template/sb_product.php'; ?>
            <?php endwhile; endif; wp_reset_query(); ?>
        </div>
    </div>
</div>
<?php if($bn){ 
 foreach ($bn as $key => $bnv) { if($key==1){ ?>
<div class="sb_banner">
    <a href="<?php echo $bnv['link']; ?>">
        <img src="<?php echo $bnv['banner']; ?>" alt="banner" class="img-responsive img-full">
    </a>
</div>
<?php } } } ?>
<div class="sb_content">
    <div class="sb_box newest">
        <h3 class="sb_title">Tin tức mới nhất</h3>
        <div class="media_posts posts">
            <?php query_posts(array('post_type' => 'post', 'posts_per_page' =>8)); ?>
            <?php if(have_posts()):while(have_posts()):the_post(); ?>
                <?php include 'template/media_post.php'; ?>
            <?php endwhile; endif; wp_reset_query(); ?>
        </div>
    </div>
</div>