<?php get_header(); // This fxn gets the header.php file and renders it ?>
<section id="breadcrumb-wrapper" class="breadcrumb-w-img">
    <div class="breadcrumb-overlay"></div>
</section>
<div class="container">
    <div class="row">      
        <div class="col-md-9 content_col">
            <?php if(have_posts()):while(have_posts()):the_post(); ?>
            <h2 class="single-title"><span><?php the_title(); ?></span></h2>
            <div class="entry_content">
                <?php the_content(); ?>
            </div>
                       
            <?php endwhile; endif; ?>
        </div>
        <div class="col-md-3 sidebar_col">
            <?php get_sidebar(); ?>
        </div>
        
    </div>
</div>
<?php get_footer(); // This fxn gets the footer.php file and renders it ?>