<?php

/* 
 * Template Name: Booking
 */
?>
<link rel='stylesheet' id='contact-form-7-css'  href='<?php echo home_url();?>/wp-content/plugins/contact-form-7/includes/css/styles.css?ver=5.2.2' type='text/css' media='all' />
<?php if(isset($_GET['pid'])) { $pid = $_GET['pid'] ?>
<div id="popup-content" class="box_order">
	<?php $_POST["name"] = $title = get_the_title($pid);  $img = get_the_post_thumbnail_url($pid) ; ?>
	<div class="row">
		<div class="col-md-5">
			<div class="box_product_info">
				<div class="thumb_product">
					<img src="<?php echo $img; ?>" alt="<?php echo $title;?>" class="img-responsive">
				</div>
				<h3 class="pop_title"><?php echo $title;?></h3>
				<div class="pro_info">
					<p>Mã sản phẩm: <?php the_field('sku',$pid); ?> </p>
					<p>Chất liệu: <?php the_field('chatlieuu',$pid); ?>  </p>
					<p>Kích thước : <?php the_field('size',$pid); ?> </p>
					<p>Tình trạng hàng: <span style="color:#3ea322;"><?php the_field('tinhtrang',$pid) ?></span> </p>
					<p>Bảo hành: <?php the_field('baohanh',$pid); ?> </p>
					<p>Miễn phí vận chuyển nội thành </p>
				</div>
			</div>
		</div>
		<div class="col-md-7">
			<div class="box_action">
				<h3 class="title_custom">Thông tin khách hàng</h3>
				<?php echo do_shortcode('[contact-form-7 id="717" title="Đặt hàng"]'); ?>
			</div>
		</div>
	</div>
</div>
<?php } ?>
<script type='text/javascript' src='<?php echo home_url();?>/wp-content/plugins/contact-form-7/includes/js/scripts.js?ver=5.2.2' id='contact-form-7-js'></script>
