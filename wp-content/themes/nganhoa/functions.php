<?php
define('IZWT_VERSION', 1.0);
/* ----------------------------------------------------------------------------------- */
/* Register main menu for Wordpress use
/*----------------------------------------------------------------------------------- */
register_nav_menus(
    array(
        'primary' => __('Primary Menu', 'fl_theme'),
        'left' => __('Left Menu', 'fl_theme'),
        'right' => __('Right Menu', 'fl_theme'),
        'footer_menu' => __('Footer Menu', 'fl_theme'),
        'sidebar_menu' => __('Sidebar Menu', 'fl_theme')
    )
);
/* ----------------------------------------------------------------------------------- */
/* Activate sidebar for Wordpress use
/*----------------------------------------------------------------------------------- */
function fl_register_sidebars() {
register_sidebar(array(// Start a series of sidebars to register
'id' => 'sidebar', // Make an ID
'name' => 'Sidebar', // Name it
'description' => 'Take it on the side...', // Dumb description for the admin side
'before_widget' => '<div>', // What to display before each widget
'after_widget' => '</div>', // What to display following each widget
'before_title' => '<h3 class="side-title">', // What to display before each widget's title
'after_title' => '</h3>', // What to display following each widget's title
'empty_title' => '', // What to display in the case of no title defined for a widget
));
}
// adding sidebars to Wordpress (these are created in functions.php)
add_action('widgets_init', 'fl_register_sidebars');
/* ----------------------------------------------------------------------------------- */
/* Enqueue Styles and Scripts
/*----------------------------------------------------------------------------------- */
function fl_scripts() {
// get the theme directory style.css and link to it in the header
    wp_enqueue_style('fl-style', get_template_directory_uri() . '/style.css', '1.0', 'all');
    wp_enqueue_style('fl-owl-css', get_template_directory_uri() . '/css/owl.carousel.min.css');
    wp_enqueue_style('fl-bootstrap-css', get_template_directory_uri() . '/css/bootstrap.min.css');
    wp_enqueue_style('fl-fontawesome', get_template_directory_uri() . '/css/font-awesome.min.css');
    wp_enqueue_style('fl-main', get_template_directory_uri() . '/css/main.css');
    wp_enqueue_style('fl-screen', get_template_directory_uri() . '/css/fl_screen.css');
// add fitvid
    wp_enqueue_script('fl-fitvid', get_template_directory_uri() . '/js/jquery.fitvids.js', array('jquery'), IZWT_VERSION, true);
    wp_enqueue_style('fl-owl-js', get_template_directory_uri() . '/js/owl.carousel.min.js');
    wp_enqueue_script('fl-theme', get_template_directory_uri() . '/js/theme.min.js', array(), IZWT_VERSION, true);
    wp_enqueue_script('fl-bootstrap-script', get_template_directory_uri() . '/js/bootstrap.min.js', array(), '1.0', true);
    wp_enqueue_script('fl-man-js', get_template_directory_uri() . '/js/main.js', array(), '1.0', true);
}
add_action('wp_enqueue_scripts', 'fl_scripts'); // Register this fxn and allow Wordpress to call it automatcally in the header
function fl_add_support_theme() {
    add_theme_support('automatic-feed-links');
    add_theme_support('post-thumbnails');
    add_theme_support('woocommerce');
    add_image_size('thumbmd', 330, 220, true);
    add_image_size('thumblg', 450, 300, true);
    add_image_size('thumbgalleryhome', 710, 474, true);
    add_image_size('thumbgallerypopup', 1000, 667, true);
}
add_action('after_setup_theme', 'fl_add_support_theme');
// pre get posts
function fl_pre_get_posts($query) {
    if ($query->is_home()) {
//do some thing
    }
}
add_action('pre_get_posts', 'fl_pre_get_posts');
// tuy chinh khi upload media
//add_filter('wp_handle_upload_prefilter', 'fl_custom_upload_filter');
//function fl_custom_upload_filter($file){
//    //$file['name'];
//    return $file;
//}
// posts short description
//admin tool bar
add_action('after_setup_theme', 'remove_admin_bar');
function remove_admin_bar() {
    if (!current_user_can('administrator') && !is_admin()) {
      show_admin_bar(false);
  }
}
//what template
function ip_admin_bar_what_template() {
    global $wp_admin_bar;
    global $template;
    if ( $template ) {
        $wp_admin_bar->add_menu( array(
            'id' => 'ip-template',
            'parent' => 'top-secondary',
            'title' => __( basename( $template ) )
        ) );
    }
}
add_action( 'wp_before_admin_bar_render', 'ip_admin_bar_what_template' );
//shortdesc
function fl_short_desc($postID = null, $limit = 45, $more = ' [...] ') {
    $excerpt = get_post_field('post_excerpt', $postID);
    $exc_spl = explode(' ', $excerpt);
    if (has_excerpt($postID)) {
        if(count($exc_spl) > $limit){
            return wp_trim_words($excerpt, $limit, $more);
        }else{
            return $excerpt;
        }
    } else {
        return wp_trim_words(get_post_field('post_content', $postID), $limit, $more);
    }
}
include_once('ajax.php');
include_once('includes/wp_bootstrap_navwalker.php');
// wp admin custom css
function fl_login_stylesheet() {
    wp_enqueue_style('fl-login', get_template_directory_uri() . '/css/fl_login.css');
}
add_action('login_enqueue_scripts', 'fl_login_stylesheet');
add_action('login_headerurl', function() {
    return home_url('/');
});
function fl_default_image($html) {
    if (empty($html)) {
        $html = '<img class="img-responsive" src="' . get_template_directory_uri() . '/images/default.jpg">';
    }
    return $html;
}
add_action('post_thumbnail_html', 'fl_default_image');
function fl_optitle($post, $class = 'page-title') {
    ?>
    <h2 class="<?php echo $class; ?>">
        <?php if (is_category()) { ?>
            <?php single_cat_title(); ?>
        <?php } elseif (is_tag()) { ?>
            Thẻ <?php single_tag_title(); ?>
        <?php } elseif (is_tax()) { ?>
            <?php single_term_title(); ?>
        <?php } elseif (is_day()) { ?>
            Ngày <?php the_time('F jS, Y'); ?>
        <?php } elseif (is_month()) { ?>
            Tháng <?php the_time('F, Y'); ?>
        <?php } elseif (is_year()) { ?>
            Năm <?php the_time('Y'); ?>
        <?php } elseif (is_author()) { ?>
            Tác giả
        <?php } elseif (isset($_GET['paged']) && !empty($_GET['paged'])) { ?>
            Blog
        <?php } ?>
    </h2>
    <?php
}
add_filter('ot_theme_options_parent_slug', 'ot_custom_subpage');
function ot_custom_subpage() {
    return 'options-general.php';
}
add_filter('ot_theme_options_menu_title', 'ot_custom_title');
function ot_custom_title() {
    return 'Tùy chỉnh của bạn';
}
// editor to taxonomy description
remove_filter('pre_term_description', 'wp_filter_kses');
remove_filter('term_description', 'wp_kses_data');
add_filter('tien_ich_edit_form_fields', 'fl_cat_description');
function fl_cat_description(){
    ?>
    <table class="form-table">
        <tr class="form-field">
            <th scope="row" valign="top"><label for="description">Mô tả chi tiết</label></th>
            <td>
                <?php
                $setting = array('wpautop' => true, 'media_buttons' => true, 'quicktags' => true, 'textarea_rows' => '15', 'textarea_name' => 'description');
                wp_editor(wp_kses_post($tag->description, ENT_QUOTES, 'UTF-8'), 'cat_description', $setting);
                ?>
                <br />
                <span class="description">Mô tả chi tiết</span>
            </td>
        </tr>
    </table>
    <?php
}
add_action('admin_head', 'fl_remove_default_category_desc');
function fl_remove_default_category_desc(){
    global $current_screen;
    if($current_screen->id == 'edit-tien_ich'){
        ?>
        <script>
            jQuery(function($){
                $('textarea#description').closest('tr.form-field').remove();
            });
        </script>
        <?php
    }
}
function the_breadcrumb() {
    global $post;
    echo '<ul id="breadcrumbs">';
    if (!is_home()) {
        echo '<li><a href="';
        echo get_option('home');
        echo '">';
        echo 'Home';
        echo '</a></li><li class="separator"> / </li>';
        if (is_category() || is_single()) {
            echo '<li>';
            the_category(' </li><li class="separator"> / </li><li> ');
            if (is_single()) {
                echo '</li><li class="separator"> / </li><li>';
                the_title();
                echo '</li>';
            }
        } elseif (is_page()) {
            if($post->post_parent){
                $anc = get_post_ancestors( $post->ID );
                $title = get_the_title();
                foreach ( $anc as $ancestor ) {
                    $output = '<li><a href="'.get_permalink($ancestor).'" title="'.get_the_title($ancestor).'">'.get_the_title($ancestor).'</a></li> <li class="separator">/</li>';
                }
                echo $output;
                echo '<strong title="'.$title.'"> '.$title.'</strong>';
            } else {
                echo '<li><strong> '.get_the_title().'</strong></li>';
            }
        }
    }
    elseif (is_tag()) {single_tag_title();}
    elseif (is_day()) {echo"<li>Archive for "; the_time('F jS, Y'); echo'</li>';}
    elseif (is_month()) {echo"<li>Archive for "; the_time('F, Y'); echo'</li>';}
    elseif (is_year()) {echo"<li>Archive for "; the_time('Y'); echo'</li>';}
    elseif (is_author()) {echo"<li>Author Archive"; echo'</li>';}
    elseif (isset($_GET['paged']) && !empty($_GET['paged'])) {echo "<li>Blog Archives"; echo'</li>';}
    elseif (is_search()) {echo"<li>Search Results"; echo'</li>';}
    echo '</ul>';
}
add_filter('use_block_editor_for_post', '__return_false');
//ad themes option
if ( function_exists( 'acf_add_options_sub_page' ) ) {
    acf_add_options_sub_page( array(
        'title'      => 'Themes Options',
        'parent'     => 'themes.php',
        'capability' => 'manage_options'
    ) );
}
function add_to_cart($pid, $sl){
    $count = count($_SESSION['cart']);
    $cart = $_SESSION['cart'];
    $trung = false;
    foreach ($cart as $key => $value) {
        if(($value['pid'] == $pid)&&($value['size'] == $size)&&($value['cl'] == $cl)){
            $_SESSION['cart'][$key]['sl']+=$sl;
            $trung = true;
        }
    }
    if(!$trung){
        $_SESSION['cart'][$count+1]['pid'] = $pid;
        $_SESSION['cart'][$count+1]['sl'] = $sl;
    }
}
function xoasp($cid){
    unset($_SESSION['cart'][$cid]);
}
function count_cart(){
    $cart = $_SESSION['cart'];
    $dem = 0;
    foreach ($cart as $key => $value) {
        $dem += $value['sl'];
    }
    return $dem;
}
function product_price($priceFloat) {
    $symbol = ' đ';
    $symbol_thousand = '.';
    $decimal_place = 0;
    $price = number_format($priceFloat, $decimal_place, '', $symbol_thousand);
    return $price;
}


// function to display number of posts.
function getPostViews($postID){
    $count_key = 'post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if($count==''){
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
        return "0";
    }
    return $count;
}
// function to count views.
function setPostViews($postID) {
    $count_key = 'post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if($count==''){
        $count = 0;
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
    }else{
        $count++;
        update_post_meta($postID, $count_key, $count);
    }
}

//Tiệp
if (!function_exists('title')) {
    function title()
    {
        if (is_home() || is_front_page()) {
            return get_bloginfo('name');
        }

        if (is_archive()) {
            $obj = get_queried_object();
            return $obj->name;
        }

        if (is_404()) {
            return __( '404 page not found', 'text_domain' );
        }

        return get_the_title();
    }
}

//ajax init map store
add_action('wp_ajax_InitDanhmuc', 'InitDanhmuc');
add_action('wp_ajax_nopriv_InitDanhmuc', 'InitDanhmuc');
function InitDanhmuc() {
    $iddanhmuc = $_POST['iddanhmuc'];

    $data = array();
    $data['array'] = array();

    $query =  new WP_Query( array(
        'post_type' => 'store',
        'tax_query' => array(
            array(
                'taxonomy' => 'local_category',
                'field' => 'id',
                'terms' => $iddanhmuc,
                'operator'=> 'IN'
            )),
        'showposts'=> 100,
        'order' => 'DESC',
        'orderby' => 'date'
    ) );
    
    $i = 0;
    if($query->have_posts()) : while ($query->have_posts() ) : $query->the_post();
        $post_id = get_the_ID();
        $post_title = get_the_title($post_id);
        $post_content = get_the_content($post_id);
        
        $store_marker_lat = get_field('store_marker_lat', $post_id);
        $store_marker_lng = get_field('store_marker_lng', $post_id);

        //array map js
        $data['array'][$i][0] .= $post_title;
        $data['array'][$i][1] .= wpautop($post_content);
        $data['array'][$i][2] .= $store_marker_lat;
        $data['array'][$i][3] .= $store_marker_lng;

        $i++; endwhile; wp_reset_postdata(); else: echo ''; endif;

        echo json_encode($data);
        die();
    }

//ajax change map store
    add_action('wp_ajax_ChangeDanhmuc', 'ChangeDanhmuc');
    add_action('wp_ajax_nopriv_ChangeDanhmuc', 'ChangeDanhmuc');
    function ChangeDanhmuc() {
        $iddanhmuc = $_POST['iddanhmuc'];

        $data = array();
        $data['array'] = array();

        $query =  new WP_Query( array(
            'post_type' => 'store',
            'tax_query' => array(
                array(
                    'taxonomy' => 'local_category',
                    'field' => 'id',
                    'terms' => $iddanhmuc,
                    'operator'=> 'IN'
                )),
            'showposts'=> 100,
            'order' => 'DESC',
            'orderby' => 'date'
        ) );
        
        $i = 0;
        if($query->have_posts()) : while ($query->have_posts() ) : $query->the_post();
            $post_id = get_the_ID();
            $post_title = get_the_title($post_id);
            $post_content = get_the_content($post_id);
            
            $store_marker_lat = get_field('store_marker_lat', $post_id);
            $store_marker_lng = get_field('store_marker_lng', $post_id);

            $data['result'] .= '
            <div class="item">
            <div class="title">
            <i class="fa fa-map-marker icon"></i>
            <a href="javascript:void(0)" data-title="'.$post_title.'" data-content="'.wpautop($post_content).'" data-lat="'.$store_marker_lat.'" data-lng="'.$store_marker_lng.'">
            <h4>'.$post_title.'</h4>
            </a>
            </div>
            </div>
            <div class="desc">'.wpautop( $post_content ).'</div>
            ';

        //array map js
            $data['array'][$i][0] .= $post_title;
            $data['array'][$i][1] .= wpautop($post_content);
            $data['array'][$i][2] .= $store_marker_lat;
            $data['array'][$i][3] .= $store_marker_lng;

            $i++; endwhile; wp_reset_postdata(); else: echo ''; endif;

            $data['count'] = $i;

            echo json_encode($data);
            die();
        }


