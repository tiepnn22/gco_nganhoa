(function ($) {
        $('.navbar .dropdown').hover(function () {
    $(this).find('.dropdown-menu').first().stop(true, true).slideDown(250);
    }, function () {
    $(this).find('.dropdown-menu').first().stop(true, true).slideUp(100)
    });
    
     $(window).scroll(function() {
        if ($(window).scrollTop() > 36) {
            $('#header').addClass('navbar-fixed-top');
        } else {
            $('#header').removeClass('navbar-fixed-top');
        }
    });
    // sidebar menu

    $('.sidebar_menu li.menu-item-has-children > a').append('<i class="fa fa-plus-circle"></i>');
    $('.sidebar_menu li.menu-item-has-children > a > i').click(function (e) {
        e.preventDefault();
        var sub_menu = $(this).closest('li').children('ul');
        if (sub_menu.css('display') === 'none') {
            $(this).removeClass('fa-plus-circle').addClass('fa-minus-circle');
            sub_menu.slideDown();
        } else {
            $(this).removeClass('fa-minus-circle').addClass('fa-plus-circle');
            sub_menu.slideUp();
        }
    });
    // main menu
    $(window).resize(function () {
        if (window.matchMedia('(min-width: 768px)').matches) {
            $('#topmenu li.dropdown').mouseenter(function () {
                $(this).addClass('open');
            });
            $('#topmenu li.dropdown').mouseleave(function () {
                $(this).removeClass('open');
            });

            $('#topmenu li.dropdown a').click(function () {
                var url = $(this).attr('href');
                window.location.href = url;
            });
        }
    }).resize();

    // fade modal
    
    $(document).ready(function(){
       setTimeout(function(){
           $('#myModal').modal('show');
       }, 5000) ;
    });
    
     var wheight = screen.height;
    $(window).scroll(function(){
        var scTop = $(window).scrollTop();
        if(scTop > (wheight/2)){
            $('#to_top').fadeIn(200);
        }else{
            $('#to_top').fadeOut(200);
        }
    });
    
    $('#to_top').click(function(e){
        e.preventDefault();
        $('body, html').animate({scrollTop: 0}, 1000);
    });
	
	//GCO
	$('.tax-product_cat .content_col .tax_desc').after('<div class="readmore"><a href="javascript:void(0)">Xem thêm</a></div>');
	$('.tax-product_cat .content_col .tax_desc').after('<div class="readmore-close"><a href="javascript:void(0)">Thu gọn</a></div>');

	$('.tax-product_cat .content_col .readmore').click(function(){
		$('.tax-product_cat .content_col .tax_desc').css('height','auto');
		$('.tax-product_cat .content_col .tax_desc').css('overflow','inherit');
			
		$('.tax-product_cat .content_col .readmore').css('display','none');
		$('.tax-product_cat .content_col .readmore-close').css('display','block');
	});
	$('.tax-product_cat .content_col .readmore-close').click(function(){
		$('.tax-product_cat .content_col .tax_desc').css('height','95px');
		$('.tax-product_cat .content_col .tax_desc').css('overflow','hidden');
			
		$('.tax-product_cat .content_col .readmore').css('display','block');
		$('.tax-product_cat .content_col .readmore-close').css('display','none');

	    $('html, body').animate({
	       scrollTop: $('.tax-product_cat .content_col .tax_desc, .tax-product_cat .content_col .tax_desc').offset().top
	    }, 1000);
	});
    
})(jQuery);


function showVideo(id, url) {
    document.getElementById(id).removeAttribute('src');
    document.getElementById(id).setAttribute('src', url);
}