<?php
/**
    *   Template Name: Home Page
*
*/
get_header();
?>
<h1 style="display: none;" class="">VẬT LIỆU TRANG TRÍ NỘI THẤT NGÂN HOA</h1>
<div id="home_slider">
    <div id="myCarousel" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
            <?php query_posts(array('post_type' => 'slide', 'posts_per_page' => -1)); ?>
            <?php $i=0; if(have_posts()):while(have_posts()):the_post(); $i++; ?>
            <?php if($i == 1 ): ?>
            <li data-target="#myCarousel" data-slide-to="<?php echo $i; ?>" class="active"></li>
            <?php else: ?>
            <li data-target="#myCarousel" data-slide-to="<?php echo $i; ?>"></li>
            <?php endif;?>
            <?php endwhile; endif; wp_reset_query(); ?>
        </ol>
        <div class="carousel-inner">
            <?php query_posts(array('post_type' => 'slide', 'posts_per_page' => -1)); ?>
            <?php $i=0; if(have_posts()):while(have_posts()):the_post(); $i++; ?>
            <div class="item <?php if ($i==1) {echo 'active' ;} else { echo 'no-active';} ?>">
                <li><?php the_post_thumbnail('full', array('class' => 'img-responsive')); ?></li>
            </div>
            <?php endwhile; endif; wp_reset_query(); ?>
        </div>
    </div>
    <a class="left carousel-control" href="#myCarousel" data-slide="prev">
        <span class=""><i class="fa fa-angle-left"></i></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#myCarousel" data-slide="next">
        <span class=""><i class="fa fa-angle-right"></i></span>
        <span class="sr-only">Next</span>
    </a>
</div>
<div class="hbox1">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <div class="hbtext">
                    <?php the_field('gt'); ?>
                </div>
            </div>
            <div class="col-md-8">
                <div class="hb1_ct">
                    <?php $hb = get_field('icon'); ?>
                    <?php foreach ($hb as $key => $hbv) { ?>
                        <div class="hb_frame">
                            <a href="<?php echo $hbv['link']; ?>">
                            <div class="hb_col">
                                <img src="<?php echo $hbv['image']; ?>" alt="<?php echo $hbv['title']; ?>" class="img-responsive">
                            </div>
                            <h4 class="hb_title"><?php echo $hbv['title']; ?></h4>
                            </a>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</div>
 
<div class="hbox3">
    <div class="container">
        <h2 class="title-box"><span>Giới thiệu</span></h2>
        <div class="row">
            <div class="col-md-3">
                <div class="hb3_thumb">
                    <img src="<?php the_field('box2_image'); ?>" alt="" class="img-responsive">
                </div>
            </div>
            <div class="col-md-3">
                <div class="hb3_content">
                    <?php the_field('box2_content'); ?>
                    <button class="btn-xemthem"><a href="<?php the_field('box2_link'); ?>">Chi tiết <i class="fa fa fa-caret-right"></i></a></button>
                </div>
            </div>
            <div class="col-md-6">
                <div class="box3_icon">
                    <?php $b3 = get_field('box2_icon'); ?>
                    <?php foreach ($b3 as $key => $b3v) { ?>
                        <div class="col-md-6">
                            <div class="b3_icon_img">
                                <a href="<?php echo $b3v['link']; ?>">
                                <img src=" <?php echo $b3v['image']; ?> " alt="<?php echo $b3v['image']; ?>" class="img-responsive">
                                <span> <?php echo $b3v['title']; ?></span>
                                </a>
                            </div>
                        </div>
                   <?php } ?>
                </div>
            </div>
        </div>
    </div>
</div>
<section id="home-parallax"> 
    <div class="container">
        <div class="inner">
            <h2 class="ntx-title text-center">Tấm ốp trần tường</h2>
            <div class="box_product">
                <?php $loop = new WP_Query(array('post_type'=>'product','posts_per_page' =>8,'tax_query'=>array(array('taxonomy'=>'product_cat','field'=>'slug','terms'=>'tam-op-tran-tuong')))); ?>
                    <div class="row">
                        <?php while ($loop->have_posts() ) : $loop->the_post();?>
                            <div class="col-md-3">
                                 <?php include 'template/product-item.php'; ?>
                            </div>
                        <?php endwhile; ?>
                    </div>
                <?php wp_reset_query(); ?>
            </div>
        </div>
    </div>
</section>
<section id="home-parallax"> 
    <div class="container">
        <div class="inner">
            <h2 class="ntx-title text-center">Phào chỉ trang trí</h2>
            <div class="box_product">
                   <?php $loop = new WP_Query(array('post_type'=>'product','posts_per_page' =>8,'tax_query'=>array(array('taxonomy'=>'product_cat','field'=>'slug','terms'=>'phao-chi-trang-tri')))); ?>
                    <div class="row">
                        <?php while ($loop->have_posts() ) : $loop->the_post();?>
                            <div class="col-md-3">
                                 <?php include 'template/product-item.php'; ?>
                            </div>
                        <?php endwhile; ?>
                    </div>
                <?php wp_reset_query(); ?>
            </div>
        </div>
    </div>
</section>
<section id="home-parallax"> 
    <div class="container">
        <div class="inner">
            <h2 class="ntx-title text-center">Khung tranh</h2>
            <div class="box_product">
                 <?php $loop = new WP_Query(array('post_type'=>'product','posts_per_page' =>8,'tax_query'=>array(array('taxonomy'=>'product_cat','field'=>'slug','terms'=>'khung-tranh')))); ?>
                    <div class="row">
                        <?php while ($loop->have_posts() ) : $loop->the_post();?>
                            <div class="col-md-3">
                                 <?php include 'template/product-item.php'; ?>
                            </div>
                        <?php endwhile; ?>
                    </div>
                <?php wp_reset_query(); ?>
            </div>
        </div>
    </div>
</section>
<section id="home-parallax"> 
    <div class="container">
        <div class="inner">
            <h2 class="ntx-title text-center">Rèm nhựa giả gỗ</h2>
            <div class="box_product">
                 <?php $loop = new WP_Query(array('post_type'=>'product','posts_per_page' =>8,'tax_query'=>array(array('taxonomy'=>'product_cat','field'=>'slug','terms'=>'rem-nhua-gia-go')))); ?>
                    <div class="row">
                        <?php while ($loop->have_posts() ) : $loop->the_post();?>
                            <div class="col-md-3">
                                 <?php include 'template/product-item.php'; ?>
                            </div>
                        <?php endwhile; ?>
                    </div>
                <?php wp_reset_query(); ?>
            </div>
        </div>
    </div>
</section>
<section id="home-parallax"> 
    <div class="container">
        <div class="inner">
            <h2 class="ntx-title text-center">Tấm gỗ nhựa thông minh</h2>
            <div class="box_product">
               <?php $loop = new WP_Query(array('post_type'=>'product','posts_per_page' =>8,'tax_query'=>array(array('taxonomy'=>'product_cat','field'=>'slug','terms'=>'tam-go-nhua-thong-minh')))); ?>
                    <div class="row">
                        <?php while ($loop->have_posts() ) : $loop->the_post();?>
                            <div class="col-md-3">
                                 <?php include 'template/product-item.php'; ?>
                            </div>
                        <?php endwhile; ?>
                    </div>
                <?php wp_reset_query(); ?>
            </div>
        </div>
    </div>
</section>
<div class="box_end">
    <div class="container">
          <h3 class="ntx-title text-center">Tin tức</h3>
            <div class="row">  
                <div class="col-md-6 kenh_video video_top">
                    <iframe width="560" height="315" id="video_top" src="https://www.youtube.com/embed/<?php the_field('big_video'); ?>" frameborder="0" allowfullscreen></iframe>
                    <div class="list_video fix_vd">
                        <ul>
                            <?php $fvideo = get_field('video'); ?>
                            <?php foreach ($fvideo as $l_video) { ?>
                            <li onclick="return showVideo('video_top','https://www.youtube.com/embed/<?php echo $l_video['id_youtube']; ?>?rel=0&autoplay=1');">
                                <img src="https://img.youtube.com/vi/<?php echo $l_video['id_youtube']; ?>/1.jpg" class="photo">
                            </li>
                            <?php } ?>
                        </ul>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="box_tintuc">
                         <?php $loop = new WP_Query(array('post_type'=>'post','posts_per_page' =>4,'category_name'=>'tin-tuc')); ?>
                                <?php while ($loop->have_posts() ) : $loop->the_post();?>
                                         <?php include 'template/row_post.php'; ?>
                                <?php endwhile; ?>
                            </div>
                        <?php wp_reset_query(); ?>
                    </div>
                </div>
            </div>
    </div>
</div>
<div class="box_ud">
    <div class="container">
        <?php $bud = get_field('ud'); ?>
        <div class="row">
            <?php foreach ($bud as $key => $budv) { ?>
            <div class="col-md-3">
                <div class="b3_icon_img">
                    <img src=" <?php echo $budv['image']; ?> " alt="<?php echo $budv['image']; ?>" class="img-responsive">
                    <span> <?php echo $budv['title']; ?></span>
                </div>
            </div>
            <?php } ?>
        </div>
    </div>
</div>
<?php  get_footer(); // This fxn gets the footer.php file and renders it ?>