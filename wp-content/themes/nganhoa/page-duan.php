<?php
/*
* Template Name:Dự án
*/
 get_header(); // This fxn gets the header.php file and renders it ?>
 <section id="breadcrumb-wrapper" class="breadcrumb-w-img">
    <div class="breadcrumb-overlay"></div>
</section>
<div class="container">
    <div class="row">
        <div class="full_col">
            <?php if(have_posts()):while(have_posts()):the_post(); ?>
            <h2 class="page-title text-center"><span><?php the_title(); ?></span></h2>
            <div class="entry_content">
                <?php the_content(); ?>
            </div>
            <?php endwhile; endif; ?>
            <?php wp_reset_query(); ?>
            <div class="box_da_list">
                <?php query_posts(array('post_type'=>'duan','posts_per_page'=>-1)); ?>
                <div class="row">
                <?php $i=0; if(have_posts()):while(have_posts()):the_post(); $i++; ?>
                    <div class="<?php if($i<=3){ echo 'col-md-4';} elseif($i<=5) { echo 'col-md-6 mdh6';} elseif($i<=8) { echo 'col-md-4';} elseif($i<=10) { echo 'col-md-6 mdh6';} elseif($i<=13){ echo 'col-md-4';} elseif($i<=15){ echo 'col-md-6 mdh6';} elseif($i<=18){ echo 'col-md-4';} elseif($i<=24){ echo 'col-md-3 mdh3';} elseif($i<=27){ echo 'col-md-4';} elseif($i<=29){ echo 'col-md-6 mdh6';} elseif($i<=32){ echo 'col-md-4';} elseif($i<=36){ echo 'col-md-3 mdh3';} elseif($i<=39){ echo 'col-md-4';} elseif($i<=41){ echo 'col-md-6 mdh6';} elseif($i<=44){ echo 'col-md-4';}  elseif($i<=48){ echo 'col-md-3 mdh3';} else { echo 'col-md-4';} ?>">
                        <div class="project_col">
                            <div class="thumb_da">
                                <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('thumblg', array('class' => 'img-responsive')); ?> </a>
                            </div>
                            <div class="wrap-product">
                                <h3 class="da_title"><?php the_title(); ?></h3>
                                <p>Diện tích : <?php  the_field('square') ?></p>
                                <p>Ngân sách : <?php the_field('ngansach') ?></p>
                                <p>Thời gian thực hiện : <?php the_field('time') ?></p>
                                <p>Chất liệu : <?php the_field('chatlieu'); ?></p>
                                <p><a href="<?php the_permalink(); ?>"><button class="btn-xem">Xem thêm</button></a></p>
                            </div>
                            </a>
                        </div>
                    </div>
                <?php endwhile;endif;wp_reset_query(); ?>
                </div>

            </div>
        </div>
    </div>
</div>
<?php get_footer(); // This fxn gets the footer.php file and renders it ?>