<div class="sb_content">
    <div class="box-sidebar tax_list">
         <h3 class="sb_title"><span>Danh mục sản phẩm</span></h3>
        <?php $cur = get_queried_object_id();?>
            <?php
            $taxonomy     = 'product_cat';
            $orderby      = 'order';
            $show_count   = 1;      // 1 for yes, 0 for no
            $pad_counts   = 1;      // 1 for yes, 0 for no
            $hierarchical = 1;      // 1 for yes, 0 for no
            $title        = '';
            $empty        = 0;
            $args = array(
            'taxonomy'     => $taxonomy,
            'orderby'      => $orderby,
            'show_count'   => $show_count,
            'pad_counts'   => $pad_counts,
            'hierarchical' => $hierarchical,
            'title_li'     => $title,
            'hide_empty'   => $empty
            );
            $all_categories = get_categories( $args ); ?>
            <ul class="products-filters">
            <?php $i=0;foreach ($all_categories as $cat) { $i++;
            if($cat->category_parent == 0) {
            $category_id = $cat->term_id;?>
            <li class="cat-item selected" ><a href="<?php echo get_term_link($cat->slug, 'product_cat') ?>"><span></span><?php echo $cat->name ?></a></li>
            <?php  $args2 = array(
              'taxonomy'=> $taxonomy,'child_of'=> 0,'parent'=> $category_id,'orderby'=> $orderby,'show_count'=> $show_count,
              'pad_counts'=> $pad_counts,'hierarchical' => $hierarchical,'title_li'=> $title,'hide_empty'=> $empty
            );
            $sub_cats = get_categories( $args2 ); if($sub_cats) { ?>
                <div class="sub_cat">
                  <?php   
                  foreach($sub_cats as $sub_category) {
                  echo  '<li class="cat-item selected"><a href="'. get_term_link($sub_category->slug, 'product_cat') .'"><span></span>'. $sub_category->name .'</a></li>' ;
                   } ?>
                </div>
                <?php }}} ?>
            </ul>
    </div>
</div>
 <?php $bn = get_field('Sidebar','option'); ?>
<?php if($bn){ 
 foreach ($bn as $key => $bnv){ ?>
<div class="sb_banner">
    <a href="<?php echo $bnv['link']; ?>">
        <img src="<?php echo $bnv['banner']; ?>" alt="banner" class="img-responsive img-full">
    </a>
</div>
<br>
<?php } }  ?>
 