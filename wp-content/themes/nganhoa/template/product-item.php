 <?php $product_id =get_the_ID(); ?>
<div class="product_item">
    <div class="thumb">
        <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('thumbpro', array('class' => 'img-responsive')); ?></a>
    </div>
    <div class="item_info">
        <h3 class="product_title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
    </div>
    <div class="box_price_item">
        <p class="text_price">Liên hệ</p>
        <div class="box_action">
            <p class="pl-std-l-view">
                  <a href="<?php the_permalink(); ?>" title="Xem nhanh">Xem nhanh</a>
            </p>
            <p class="pl-std-l-orders">
                <a data-fancybox data-type="ajax" data-src="<?php echo home_url();?>/dat-hang/?pid=<?php echo $product_id;?>" href="javascript:;"  class="fcybox-order">Mua ngay</a>
            </p>
        </div>
    </div>
</div>