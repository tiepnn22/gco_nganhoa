<div class="post_home">
    <div class="row">
        <div class="col-md-3 ">
            <div class="thumb">
                <?php  the_post_thumbnail('thumbmd', array('class' => 'img-responsive')); ?>
            </div>
        </div>
        <div class="col-md-9 post_desc">
            <div class="inner">
                <h3 class="ptitle"><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h3>
                <div class="excerpt">
                      <?php echo fl_short_desc(get_the_ID(),20,'...'); ?>
                </div>
            </div>
        </div>
    </div>
</div>

