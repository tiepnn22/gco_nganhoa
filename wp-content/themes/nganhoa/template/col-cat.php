<div class="post_list_col">
    <?php $date = get_the_date(); ?>
    <div class="thumb">
        <a href="<?php the_permalink();?>"><?php the_post_thumbnail('thumbnews_archi', array('class' => 'img-responsive')); ?></a>
    </div>
    <div class="news_title">
        <a href="<?php the_permalink(); ?>"><h3 class="news_title_slider"><?php the_title(); ?></h3></a>
    </div>
    <div class="sub_single_title">
        <div class="date"><i class="fa fa-calendar" aria-hidden="true"></i> <?php echo $date; ?></div>
    </div>
    <div class="news_exrept">
        <?php   echo fl_short_desc(get_the_ID(),25,'...'); ?>
    </div>
</div>