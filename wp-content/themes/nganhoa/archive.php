<?php get_header(); ?>
<section id="breadcrumb-wrapper" class="breadcrumb-w-img">
    <div class="breadcrumb-overlay"></div>
</section>
<div class="container">
        <div class="archive_box">
            <?php if (have_posts()) : ?>
            <?php $post = $posts[0]; // Hack. Set $post so that the_date() works. ?>
            
            <!-- <?php fl_optitle($post, 'page-title'); ?> -->
            <h1 class="page-title"><?php echo single_cat_title(); ?></h1>

            <?php if (is_tax()) { ?>
            <div class="tax_desc">
                <?php echo term_description(); ?>
            </div>
            <?php } ?>
           
            <div class="blog-single-slider row">
                <?php if(have_posts()) : while (have_posts()) : the_post(); ?>
                    <div class="col-md-4">
                        <?php include 'template/col-cat.php'; ?>
                    </div>
                <?php endwhile; endif; ?>
            </div>
            <div class="paginate">
                <?php
                global $wp_query;
                $big = 999999999; // need an unlikely integer
                echo paginate_links(array(
                'base' => str_replace($big, '%#%', esc_url(get_pagenum_link($big))),
                'format' => '?paged=%#%',
                'current' => max(1, get_query_var('paged')),
                'total' => $wp_query->max_num_pages,
                'prev_text'          => __('&larr;'),
                'next_text'          => __('&rarr;'),
                ));
                ?>
            </div>
            <?php wp_reset_query(); ?>
            <?php else : ?>
            
            <!-- <h2 class="page-title">Không có bài viết nào</h2> -->
            <h1 class="page-title"><?php echo single_cat_title(); ?></h1>

            <?php endif; ?>
        </div>
</div>
<?php get_footer(); ?>